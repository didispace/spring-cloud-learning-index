# Spring Cloud 中文学习指南

访问地址：[http://www.springcloud.com.cn/](http://www.springcloud.com.cn/)

#### 贡献内容

如果您有内容希望出现在上面的站点中，与所有Spring Cloud爱好者分享学习的话，可以[创建Issue](https://gitee.com/didispace/spring-cloud-learning-index/issues/new)，提交收录请求。

为方便管理，您需根据如下格式进行登记录入：

```markdown
## 收录申请

收录类型：优质博客、开源项目、推荐书籍
收录名称：推荐资源的名称
收录地址：推荐资源的访问地址
其他信息：可以介绍一下这个资源信息，你的推荐理由等
```

![](https://images.gitee.com/uploads/images/2020/0120/135042_3b8a4013_437188.png)

